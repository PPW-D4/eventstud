from django.contrib.auth.models import User
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import historyPage
from .views import dataRegistered

# Create your tests here.
class NewspageUnitTest(TestCase):
	def test_url_history_redirect_unauthenticated_user(self):
		response = Client().get('/history/')
		self.assertEqual(response.status_code, 302)

	def test_historyPage_is_exist(self):
		found = resolve('/history/')
		self.assertEqual(found.func, historyPage)

	def test_history_using_historypage_template(self):
		client = Client()
		user = User(username="kpw")
		user.set_password("PPW2018")
		user.save()
		client.login(username="kpw", password="PPW2018")
		response = client.get('/history/')
		self.assertTemplateUsed(response, 'history/history-page.html')

	def test_dataRegistered_is_exist(self):
		found = resolve('/history/data/')
		self.assertEqual(found.func, dataRegistered)

	def test_url_data_is_exist(self):
		client = Client()
		response = client.get('/history/data/')
		self.assertEqual(response.status_code, 302)
		user = User(username="kpw")
		user.set_password("PPW2018")
		user.save()
		client.login(username="kpw", password="PPW2018")
		response = client.get('/history/data/')
		self.assertEqual(response.status_code, 200)

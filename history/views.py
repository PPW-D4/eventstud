from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
response = {}
@login_required
def dataRegistered(request):
	return JsonResponse(request.user.attendee.serialize())

def historyPage(request):
	if (request.user.is_authenticated):
		return render(request, 'history/history-page.html', response)
	else:
		return HttpResponseRedirect(reverse("social:begin", args=["google-oauth2"]))

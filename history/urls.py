from .views import historyPage
from .views import dataRegistered
from django.conf.urls import url

app_name = 'history'
urlpatterns = [
	url(r'^$', historyPage, name = 'historyPage'),
	url(r'^data/', dataRegistered, name = 'dataRegistered'),
	
]
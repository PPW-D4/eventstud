from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Attendee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    events = models.ManyToManyField('Event', blank=True)

    @classmethod
    def list_all(cls, events=True):
        return {
            "attendees": [
                attendee.serialize() for attendee in cls.objects.all()
            ]
        }

    def __str__(self):
        return f"{self.user.username} ({self.user.email})"

    def serialize(self, events=True):
        data = {
            "id": self.id,
            "username": self.user.username,
            "full_name": self.user.get_full_name(),
            "email": self.user.email,
        }
        if events:
            data["events"] = [
                event.serialize(attendees=False)
                for event in self.event_set.all()
            ]
        return data


class Event(models.Model):
    title = models.CharField("title", max_length=64)
    fee = models.CharField("fee", max_length=64)
    time_start = models.DateTimeField("time start")
    time_end = models.DateTimeField("time end")
    venue = models.CharField("venue", max_length=128)
    main_image_url = models.CharField("main image URL", max_length=1999)
    thumbnail_image_url = models.CharField(
        "thumbnail image URL", max_length=1999
    )
    description = models.TextField("description", max_length=640)
    organizer_email = models.EmailField("email", max_length=128, blank=True)
    attendees = models.ManyToManyField(
        'Attendee', through=Attendee.events.through, blank=True
    )

    @classmethod
    def list_all(cls, attendees=True):
        return {
            "events": [
                event.serialize() for event in cls.objects.all()
            ]
        }

    def __str__(self):
        return f"{self.title} @ {self.venue}"

    def clean(self):
        cleaned_data = super(Event, self).clean()
        if self.time_start > self.time_end:
            raise ValidationError(
                "Event start time should be before event end time"
        )

    def serialize(self, attendees=True):
        data = {
            "id": self.id,
            "title": self.title,
            "fee": self.fee,
            "time_start": str(self.time_start),
            "time_end": str(self.time_end),
            "venue": self.venue,
            "main_image_url": self.main_image_url,
            "thumbnail_image_url": self.thumbnail_image_url,
            "description": self.description,
            "organizer_email": self.organizer_email,
        }
        if attendees:
            data["attendees"] = [
                attendee.serialize(events=False)
                for attendee in self.attendee_set.all()
            ]
        return data


@receiver(post_save, sender=User)
def create_user_attendee(sender, instance, created, **kwargs):
    if created:
        Attendee.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_attendee(sender, instance, **kwargs):
    instance.attendee.save()

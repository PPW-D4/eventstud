from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models, IntegrityError
from django.test import TestCase
from django.urls import resolve, reverse
from django.utils import timezone

from events2.models import Attendee, Event
from events2.views import index, detail


def create_event(
    title="PPW CodeJam", fee="FREE",
    time_start=timezone.now(),
    time_end=timezone.now() + timezone.timedelta(hours=5),
    venue="Sekre Fasilkom UI",
    main_image_url="https://via.placeholder.com/1920x1080",
    thumbnail_image_url="https://via.placeholder.com/640x320",
    description="y x g q",
    organizer_email="pewe@cs.ui.ac.id"
):
    return Event(
            title=title,
            fee=fee,
            time_start=time_start,
            time_end=time_end,
            main_image_url=main_image_url,
            thumbnail_image_url=thumbnail_image_url,
            venue=venue,
            description=description
    )

def create_user(
        username="kpw",
        password="pepewe@cs.ui",
        first_name="Kak",
        last_name="Pewe",
        email="pw@cs.ui.ac.id"
    ):
    user = User(
        username=username,
        first_name=first_name,
        last_name=last_name,
        email=email
    )
    user.set_password(password)
    return user


class EventsTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user = create_user()
        self.user.save()
        self.event = create_event()
        self.event.save()


class EventModelsTestCase(EventsTestCase):
    def test_model_can_create_new_event(self):
        new_event = create_event()
        new_event.save()
        all_events_count = Event.objects.all().count()
        self.assertEqual(all_events_count, 2)

    def test_model_time_start_after_time_end(self):
        new_event = create_event(
            time_start=timezone.now() + timezone.timedelta(hours=5),
            time_end=timezone.now()
        )
        self.assertRaises(ValidationError, new_event.full_clean)

    def test_model_can_create_new_attendee(self):
        new_user = create_user("dekdepe", "ddp@cs.ui")
        new_user.save()
        all_attendees_count = Attendee.objects.all().count()
        self.assertEqual(all_attendees_count, 2)

    def test_model_can_add_attendee_to_event(self):
        self.event.attendees.add(self.user.attendee)
        all_attendees_count_in_new_event = self.event.attendees.all().count()
        self.assertEqual(all_attendees_count_in_new_event, 1)

    def test_model_event_string(self):
        new_event = create_event()
        self.assertEqual(
            str(new_event), f"{new_event.title} @ {new_event.venue}"
        )

    def test_model_attendee_string(self):
        self.assertEqual(
            str(self.user.attendee),
            f"{self.user.username} ({self.user.email})"
        )

    def test_event_serialize(self):
        new_user = create_user(username="dekdepe", email="ddp@cs.ui.ac.id")
        new_user.save()
        self.event.attendees.add(self.user.attendee, new_user.attendee)
        self.assertEqual(
            self.event.serialize(), {
                "id": self.event.id,
                "title": self.event.title,
                "fee": self.event.fee,
                "time_start": str(self.event.time_start),
                "time_end": str(self.event.time_end),
                "venue": self.event.venue,
                "main_image_url": self.event.main_image_url,
                "thumbnail_image_url": self.event.thumbnail_image_url,
                "description": self.event.description,
                "organizer_email": self.event.organizer_email,
                "attendees": [
                    self.user.attendee.serialize(events=False),
                    new_user.attendee.serialize(events=False)
                ]
            }
        )

    def test_attendee_serialize(self):
        new_event = create_event(title="KL SDA")
        new_event.save()
        self.user.attendee.events.add(self.event, new_event)
        self.assertEqual(
            self.user.attendee.serialize(), {
                "id": self.user.attendee.id,
                "username": self.user.attendee.user.username,
                "full_name": self.user.attendee.user.get_full_name(),
                "email": self.user.attendee.user.email,
                "events": [
                    self.event.serialize(attendees=False),
                    new_event.serialize(attendees=False)
                ]
            }
        )

    def test_event_list_all(self):
        events = [
            self.event,
            create_event(title="PPW FEST"),
            create_event(title="KL SDA")
        ]
        for event in events:
            event.save()
        self.assertEqual(
            Event.list_all(), {
                "events" : [
                    event.serialize() for event in events
                ]
            }
        )

    def test_attendee_list_all(self):
        users = [
            self.user,
            create_user(username="dekdepe", email="ddp@cs.ui.ac.id"),
            create_user(username="sisdea", email="sda@cs.ui.ac.id")
        ]
        for user in users:
            user.save()
        self.assertEqual(
            Attendee.list_all(), {
                "attendees": [
                    user.attendee.serialize() for user in users
                ]
            }
        )


class EventViewsTestCase(EventsTestCase):
    def test_events_url_exists(self):
        response = self.client.get(reverse('events2:index'))
        self.assertEqual(response.status_code, 200)

    def test_events_uses_index_function(self):
        found = resolve(reverse('events2:index'))
        self.assertEqual(found.func, index)

    def test_events_uses_index_template(self):
        response = self.client.get(reverse('events2:index'))
        self.assertTemplateUsed(response, 'events2/index.html')

    def test_event_available(self):
        response = self.client.get(reverse('events2:index'))
        content = response.content.decode('utf8')
        self.assertNotIn('No events available.', content)
        self.assertIn('<table', content)

    def test_no_events_available(self):
        self.event.delete()
        response = self.client.get(reverse('events2:index'))
        content = response.content.decode('utf8')
        self.assertIn('No events available.', content)
        self.assertNotIn('<table', content)

    def test_event_detail_url_exists(self):
        response = self.client.get(
            reverse('events2:detail', kwargs={'pk': self.event.id})
        )
        content = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn("PPW CodeJam", content)

    def test_event_detail_uses_detail_function(self):
        found = resolve(
            reverse('events2:detail', kwargs={'pk': self.event.id})
        )
        self.assertEqual(found.func, detail)

    def test_event_detail_uses_detail_template(self):
        response = self.client.get(
            reverse('events2:detail', kwargs={'pk': self.event.id})
        )
        self.assertTemplateUsed(response, 'events2/detail.html')

    def test_event_detail_no_attendees_available(self):
        response = self.client.get(
            reverse('events2:detail', kwargs={'pk': self.event.id})
        )
        content = response.content.decode('utf8')
        self.assertIn('No attendees yet.', content)
        self.assertNotIn('<table', content)

    def test_register_post_forbidden(self):
        response = self.client.post(
            reverse('events2:register', kwargs={'pk': self.event.id}),
            data={"mode": "register"}
        )
        self.assertEqual(response.status_code, 403)

    def test_register_post_success_and_render_the_result(self):
        self.client.login(username="kpw", password="pepewe@cs.ui")
        self.client.post(
            reverse('events2:register', kwargs={'pk': self.event.id})
        )
        response = self.client.get(
            reverse('events2:detail', kwargs={'pk': self.event.id})
        )
        content = response.content.decode('utf8')
        self.assertNotIn('No attendees yet.', content)
        self.assertIn(self.user.username, content)

    def test_unregister_post_success_and_render_the_result(self):
        self.client.login(username="kpw", password="pepewe@cs.ui")
        self.client.post(
            reverse('events2:register', kwargs={'pk': self.event.id})
        )
        self.client.post(
            reverse('events2:register', kwargs={'pk': self.event.id})
        )
        self.client.logout()
        response = self.client.get(
            reverse('events2:detail', kwargs={'pk': self.event.id})
        )
        content = response.content.decode('utf8')
        self.assertIn('No attendees yet.', content)
        self.assertNotIn(self.user.username, content)

    def test_attendee_can_register_to_two_events(self):
        self.client.login(username="kpw", password="pepewe@cs.ui")
        self.client.post(
            reverse('events2:register', kwargs={'pk': self.event.id})
        )
        new_event = create_event()
        new_event.save()
        self.client.post(
            reverse('events2:register', kwargs={'pk': new_event.id})
        )
        self.assertEqual(
            self.user.attendee.events.all().count(), 2
        )

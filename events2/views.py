from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import render_to_string
from django.urls import reverse
from events2.models import Attendee, Event


def index(request):
    event_list = Event.objects.all().order_by('time_start')
    context = {'event_list': event_list}
    return render(request, 'events2/index.html', context)


def detail(request, pk):
    context = {}
    context['event'] = event = get_object_or_404(Event, pk=pk)
    if request.user.is_authenticated:
        user_is_registered = request.user.attendee in event.attendee_set.all()
        context['mode'] = "unregister" if user_is_registered else "register"
    else:
        context['mode'] = "login"

    return render(request, 'events2/detail.html', context)


def register(request, pk):
    event = get_object_or_404(Event, pk=pk)
    if request.method == 'POST':
        if not request.user.is_authenticated:
            return HttpResponseForbidden("Forbidden")
        user_is_registered = request.user.attendee in event.attendee_set.all()
        if not user_is_registered:
            event.attendees.add(request.user.attendee)
        else:
            event.attendees.remove(request.user.attendee)
    data = {
        'html': render_to_string(
            'events2/attendee-list.html', {'event': event}
        )
    }
    return JsonResponse(data)

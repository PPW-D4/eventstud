from django.urls import include, path
from . import views


app_name = 'events2'
urlpatterns = [
    path('', views.index, name="index"),
    path('<int:pk>/', views.detail, name='detail'),
    path('<int:pk>/register/', views.register, name='register')
]

from django.contrib import admin
from events2.models import Attendee, Event

admin.site.register(Attendee)
admin.site.register(Event)

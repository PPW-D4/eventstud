$(document).ready(function(){
    $('#register-form').submit(function(event) {
        event.preventDefault();
        var formData = $(this).serializeArray();
        $.ajax({
            type: 'POST',
            url: 'register/',
            data: formData,
            dataType: 'json',
        }).then(function(response) {
            $("#attendee-list").html(response.html);
            if ($("#register-button").text().trim() === "REGISTER") {
                $("#register-button").text("UNREGISTER");
            } else {
                $("#register-button").text("REGISTER");
            }
        })
    })
});

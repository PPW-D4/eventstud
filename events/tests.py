from django.core.exceptions import ValidationError
from django.db import models, IntegrityError
from django.test import TestCase
from django.urls import resolve, reverse
from django.utils import timezone

from events.forms import AttendeeForm
from events.models import Attendee, Event
from events.views import index, detail


def create_event(
    title="PPW CodeJam", fee="FREE",
    time_start=timezone.now(),
    time_end=timezone.now() + timezone.timedelta(hours=5),
    main_image_url="https://via.placeholder.com/1920x1080",
    thumbnail_image_url="https://via.placeholder.com/640x320",
    venue="Sekre Fasilkom UI", description="y x g q"
):
    return Event(
            title=title,
            fee=fee,
            time_start=time_start,
            time_end=time_end,
            main_image_url=main_image_url,
            thumbnail_image_url=thumbnail_image_url,
            venue=venue,
            description=description
    )


def create_attendee(
    username="kpw",
    password="Cihuy1234",
    email="pw@cs.ui.ac.id"):
    return Attendee(
            username=username,
            password=password,
            email=email
        )


def create_attendee_form(
    username="kpw",
    password="Cihuy1234",
    email="pw@cs.ui.ac.id"):
    return AttendeeForm(
        data={'username': username, 'password': password, 'email': email}
    )


class EventModelsTestCase(TestCase):
    def test_model_can_create_new_event(self):
        new_event = create_event()
        new_event.save()
        all_events_count = Event.objects.all().count()
        self.assertEqual(all_events_count, 1)

    def test_model_time_start_after_time_end(self):
        new_event = create_event(
            time_start=timezone.now() + timezone.timedelta(hours=5),
            time_end=timezone.now()
        )
        self.assertRaises(ValidationError, new_event.full_clean)

    def test_model_can_create_new_attendee(self):
        new_attendee = create_attendee()
        new_attendee.save()
        all_attendees_count = Attendee.objects.all().count()
        self.assertEqual(all_attendees_count, 1)

    def test_model_can_add_attendee_to_event(self):
        new_event = create_event()
        new_event.save()
        new_attendee = create_attendee()
        new_attendee.save()
        new_event.attendees.add(new_attendee)
        all_attendees_count_in_new_event = new_event.attendees.all().count()
        self.assertEqual(all_attendees_count_in_new_event, 1)

    def test_model_event_string(self):
        new_event = create_event()
        self.assertEqual(
            f"{new_event.title} @ {new_event.venue}", str(new_event)
        )

    def test_model_attendee_string(self):
        new_attendee = create_attendee()
        self.assertEqual(
            f"{new_attendee.username} ({new_attendee.email})", str(new_attendee)
        )


class EventFormsTestCase(TestCase):
    def test_form_attendee_username_is_unique(self):
        attendee1 = create_attendee_form(
            username="kakpewe", password="Cihuy1234", email="pw@cs.ui.ac.id"
        )
        attendee1.save()
        attendee2 = create_attendee_form(
            username="kakpewe", password="Cihuy5678", email="kakpewe@cs.ui.ac.id"
        )
        self.assertFalse(attendee2.is_valid())

    def test_form_attendee_email_is_unique(self):
        attendee1 = create_attendee_form(
            username="kakpewe", password="Cihuy1234", email="orang@cs.ui.ac.id"
        )
        attendee1.save()
        attendee2 = create_attendee_form(
            username="dekdepe", password="Cihuy5678", email="orang@cs.ui.ac.id"
        )
        self.assertFalse(attendee2.is_valid())

    def test_form_attendee_password_minimum_8_alphanumeric(self):
        attendee1 = create_attendee_form(password="yoi123")
        self.assertFalse(attendee1.is_valid())
        attendee2 = create_attendee_form(password="mantap gan!")
        self.assertFalse(attendee2.is_valid())
        attendee3 = create_attendee_form()
        self.assertTrue(attendee3.is_valid())

    def test_form_event_validation_for_blank_items(self):
        form = create_attendee_form(username='', password='', email='')
        self.assertFalse(form.is_valid())
        for field in form:
            self.assertEqual(
                form.errors[field.name],
                ["This field is required."]
            )

class EventViewsTestCase(TestCase):
    def test_events_url_exists(self):
        response = self.client.get(reverse('events:index'))
        self.assertEqual(response.status_code, 200)

    def test_events_uses_index_function(self):
        found = resolve(reverse('events:index'))
        self.assertEqual(found.func, index)

    def test_events_uses_index_template(self):
        response = self.client.get(reverse('events:index'))
        self.assertTemplateUsed(response, 'events/index.html')

    def test_no_events_available(self):
        response = self.client.get(reverse('events:index'))
        content = response.content.decode('utf8')
        self.assertIn('No events available.', content)
        self.assertNotIn('<table', content)

    def test_event_detail_url_exists(self):
        new_event = create_event()
        new_event.save()
        response = self.client.get(
            reverse('events:detail', kwargs={'pk': new_event.id})
        )
        content = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn("PPW CodeJam", content)

    def test_event_detail_uses_detail_function(self):
        new_event = create_event()
        new_event.save()
        found = resolve(reverse('events:detail', kwargs={'pk': new_event.id}))
        self.assertEqual(found.func, detail)

    def test_event_detail_uses_detail_template(self):
        new_event = create_event()
        new_event.save()
        response = self.client.get(
            reverse('events:detail', kwargs={'pk': new_event.id})
        )
        self.assertTemplateUsed(response, 'events/detail.html')

    def test_event_detail_no_attendees_available(self):
        new_event = create_event()
        new_event.save()
        response = self.client.get(
            reverse('events:detail', kwargs={'pk': new_event.id})
        )
        content = response.content.decode('utf8')
        self.assertIn('No attendees yet.', content)
        self.assertNotIn('<table', content)

    def test_register_to_event_form_in_event_detail(self):
        new_event = create_event()
        new_event.save()
        response = self.client.get(
            reverse('events:detail', kwargs={'pk': new_event.id})
        )
        content = response.content.decode('utf8')
        self.assertIn('<form', content)
        self.assertIn('method="post">', content)
        self.assertIn(' for="email">Email</label>', content)
        self.assertIn(' for="username">Username</label>', content)
        self.assertIn('<input type="text"', content)
        self.assertIn(' for="password">Password</label>', content)
        self.assertIn('<input type="password"', content)
        self.assertIn('id="id_username"', content)

    def test_register_post_success_and_render_the_result(self):
        new_event = create_event()
        new_event.save()
        form = create_attendee_form()
        response_post = self.client.post(
            reverse('events:detail', kwargs={'pk': new_event.id}),
            {key: form[key].value() for key in form.fields}
        )
        self.assertEqual(response_post.status_code, 302)

        response = self.client.get(
            reverse('events:detail', kwargs={'pk': new_event.id})
        )
        content = response.content.decode('utf8')
        username = form['username'].value()
        self.assertNotIn('No attendees yet.', content)
        self.assertIn(username, content)

    def test_attendee_can_register_to_two_events(self):
        new_event = create_event()
        new_event.save()
        form = create_attendee_form()
        response_post = self.client.post(
            reverse('events:detail', kwargs={'pk': new_event.id}),
            {key: form[key].value() for key in form.fields}
        )
        new_event = create_event()
        new_event.save()
        response_post = self.client.post(
            reverse('events:detail', kwargs={'pk': new_event.id}),
            {key: form[key].value() for key in form.fields}
        )
        response = self.client.get(
            reverse('events:detail', kwargs={'pk': new_event.id})
        )
        content = response.content.decode('utf8')
        username = form['username'].value()
        self.assertNotIn('No attendees yet.', content)
        self.assertIn(username, content)

    def test_blank_items_in_register_event_form(self):
        new_event = create_event()
        new_event.save()
        form = create_attendee_form('', '', '')
        response_post = self.client.post(
            reverse('events:detail', kwargs={'pk': new_event.id}),
            {key: form[key].value() for key in form.fields}
        )
        content = response_post.content.decode('utf8')
        self.assertIn("This field is required.", content)

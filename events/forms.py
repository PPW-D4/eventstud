from django import forms
from events.models import Attendee


class AttendeeForm(forms.ModelForm):
    class Meta:
        model = Attendee
        fields = ['email', 'username', 'password']
        widgets = {
            'password': forms.TextInput(attrs={'type': 'password'})
        }

    def __init__(self, *args, **kwargs):
        super(AttendeeForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control form-control-custom'
            visible.field.label_suffix = ''

from django.core.exceptions import ValidationError


def validate_minimum_alphanumeric(string, name="Password", minimum=8):
    if len(string) < minimum:
        raise ValidationError(
            f"{name} length shouldn't be less than {minimum} characters."
        )

    if not string.isalnum():
        raise ValidationError(
            f"{name} should be alphanumeric."
        )

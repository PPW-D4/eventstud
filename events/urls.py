from django.urls import include, path
from . import views


app_name = 'events'
urlpatterns = [
    path('', views.index, name="index"),
    path('<int:pk>/', views.detail, name='detail')
]

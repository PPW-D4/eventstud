from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from events.forms import AttendeeForm
from events.models import Attendee, Event


def index(request):
    event_list = Event.objects.all().order_by('time_start')
    context = {'event_list': event_list}
    return render(request, 'events/index.html', context)


def detail(request, pk):
    context = {}
    context['event'] = event = get_object_or_404(Event, pk=pk)
    if request.method == 'POST':
        context['form'] = form = AttendeeForm(request.POST)
        query = Attendee.objects.filter(
            email=form['email'].value(),
            username=form['username'].value(),
            password=form['password'].value()
        )
        try:
            attendee = query.get()
        except (Attendee.DoesNotExist):
            if form.is_valid():
                event.attendees.add(form.save())
            else:
                context['anchor'] = 'register'
                return render(request, 'events/detail.html', context)
        else:
            event.attendees.add(attendee)

        return redirect(
            f"{reverse('events:detail', kwargs={'pk': pk})}#attendees"
        )

    else:
        context['form'] = form = AttendeeForm()

    return render(request, 'events/detail.html', context)

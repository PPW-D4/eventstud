# Generated by Django 2.1.1 on 2018-10-17 07:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_event_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='fee',
            field=models.CharField(default='FREE', max_length=64, verbose_name='Fee'),
            preserve_default=False,
        ),
    ]

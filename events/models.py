from django.core.exceptions import ValidationError
from django.db import models
from events.validators import validate_minimum_alphanumeric


class Attendee(models.Model):
    username = models.CharField("Username", max_length=32, unique=True)
    password = models.CharField(
        "Password", max_length=32, validators=[validate_minimum_alphanumeric]
    )
    email = models.EmailField("Email", max_length=128, unique=True)
    events = models.ManyToManyField(
        'Event', blank=True
    )

    def __str__(self):
        return f"{self.username} ({self.email})"


class Event(models.Model):
    title = models.CharField("Title", max_length=64)
    fee = models.CharField("Fee", max_length=64)
    time_start = models.DateTimeField("Time start")
    time_end = models.DateTimeField("Time end")
    venue = models.CharField("Venue", max_length=128)
    main_image_url = models.CharField(max_length=1999)
    thumbnail_image_url = models.CharField(max_length=1999)
    description = models.TextField("Description", max_length=640)
    organizer_email = models.EmailField("Email", max_length=128, blank=True)
    attendees = models.ManyToManyField(
        'Attendee', through=Attendee.events.through, blank=True
    )

    def __str__(self):
        return f"{self.title} @ {self.venue}"

    def clean(self):
        cleaned_data = super(Event, self).clean()
        if self.time_start > self.time_end:
            raise ValidationError(
                "Event start time should be before event end time"
        )

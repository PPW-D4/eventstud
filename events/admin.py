from django.contrib import admin
from events.models import Attendee, Event

admin.site.register(Attendee)
admin.site.register(Event)

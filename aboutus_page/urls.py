from django.urls import path
from .views import *

app_name = 'aboutus_page'
urlpatterns = [
    path('', index, name="index"),
    path('save/', save_testimoni, name="save_testimoni")
]
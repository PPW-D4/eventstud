from django.test import TestCase
from django.http import request
from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import *
from .models import *


# Create your tests here.
class AboutUsUnitTest(TestCase):
    def test_about_us_page_url_is_exist(self):
        response = Client().get('/aboutUs/')
        self.assertEqual(response.status_code, 200)

    def test_about_page_using_templates(self):
        response = Client().get('/aboutUs/')
        self.assertTemplateUsed(response, 'aboutUs.html')
        
    def test_about_us_page_using_index_func(self):
        found = resolve('/aboutUs/')
        self.assertEqual(found.func, index)

    def test_testimoni_model_can_add_new(self):
        Testimoni.objects.create(name='test', text='testhalo123')
        testimoni_counts_all_entries = Testimoni.objects.all().count()
        self.assertEqual(testimoni_counts_all_entries, 1)

    def test_self_func_name(self):
        Testimoni.objects.create(name='Jihan', text='HaloJihan')
        testimoni = Testimoni.objects.get(name='Jihan')
        self.assertEqual(str(testimoni), testimoni.name)

    def test_text_max_name_length(self):
        name = Testimoni.objects.create(name='Jihan', text='HaloJihan')
        self.assertLessEqual(len(str(name)), 60)

    def test_about_us_page_using_save_testimoni_func(self):
        found = resolve('/aboutUs/save/')
        self.assertEqual(found.func, save_testimoni)
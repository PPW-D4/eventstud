from django.apps import AppConfig


class AboutusPageConfig(AppConfig):
    name = 'aboutus_page'

from django.shortcuts import render
from django.http import JsonResponse
from .models import *

# Create your views here.
content = {}

def index(request):
    data = list(Testimoni.objects.all())
    data.reverse()
    content['data'] = data
    return render(request, 'aboutUs.html', content)

def save_testimoni(request):
    if request.method == 'POST':
        text = request.POST['text']
        name = request.user.first_name + " " + request.user.last_name
        Testimoni.objects.create(name=name, text=text)
        return JsonResponse({'saved': True, 'name': name, 'text': text})
    return JsonResponse({'saved': False})
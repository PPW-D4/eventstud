from django.urls import path
from . import views

app_name = 'home'
urlpatterns = [
    path('', views.index, name="index"),
    path('logout/', views.logout , name='logout'),
    path('is_login/', views.is_login, name ='is_login'),
]

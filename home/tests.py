from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse, resolve
from . import views

class HomeTestCase(TestCase):
    def test_home_url_exists(self):
        response = self.client.get(reverse('home:index'))
        self.assertEqual(response.status_code, 200)

    def test_home_uses_index_function(self):
        found = resolve(reverse('home:index'))
        self.assertEqual(found.func, views.index)

    def test_text_in_home(self):
        text = "MARK YOUR DATE!"
        response = self.client.get(reverse('home:index'))
        content = response.content.decode('utf8')
        self.assertIn(text, content)

    def test_user_can_logout(self):
        user = User(username = 'adindahawari')
        user.set_password('hahahihihuhu')
        user.save()
        self.client.login(username = 'adindahawari', password = 'hahahihihuhu')
        response = self.client.get(reverse('home:is_login'))
        self.assertJSONEqual(response.content, {'login': True})
        self.client.get(reverse('home:logout'))
        response = self.client.get(reverse('home:is_login'))
        self.assertJSONEqual(response.content, {'login': False})

from django.contrib.auth import logout as auth_logout
from django.shortcuts import render
from events2.models import Event
from django.http import HttpResponseRedirect, JsonResponse

def index(request):
    response = {}
    response["event_list"] = Event.objects.all()[:3]
    return render(request, 'home/hello.html', response)

def logout(request):
	if request.user.is_authenticated:
		auth_logout(request)
	return HttpResponseRedirect('/')

def is_login(request):
	if request.user.is_authenticated:
		return JsonResponse({'login': True})
	return JsonResponse({'login': False})
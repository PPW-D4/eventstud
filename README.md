# EventStud

[![pipeline status](https://gitlab.com/PPW-D4/eventstud/badges/master/pipeline.svg)](https://gitlab.com/PPW-D4/eventstud/commits/master)
[![coverage report](https://gitlab.com/PPW-D4/eventstud/badges/master/coverage.svg)](https://gitlab.com/PPW-D4/eventstud/commits/master)

## Tugas Perancangan dan Pemrograman Web 2018

### Fakultas Ilmu Komputer, Universitas Indonesia

#### Kelompok 4 - Kelas D

| **No.** | **NPM**    | **Nama**                    | ***Branch* tugas 1**       | ***Branch* tugas 2**         |
| ------- | ---------- | --------------------------- | -------------------------- | ---------------------------- |
| **1.**  | 1706021732 | Adinda Raisha Hanief Hawari | [**`home`**][home]         | [**`dinda`**][dinda]         |
| **2.**  | 1706043683 | Saffanah Fausta Lamis       | [**`newspage`**][newspage] | [**`saffa`**][saffa]         |
| **3.**  | 1706979455 | Sage Muhammad Abdullah `*`  | [**`events`**][events]     | [**`events-v2`**][events_v2] |
| **4.**  | 1706984644 | Jihan Maulana Octa          | [**`register`**][register] | [**`about_us`**][about_us]   |

`*` Repository maintainer

### [Live app on heroku](https://eventstud.herokuapp.com)

### [Live (dev) app on heroku](https://eventstud-dev.herokuapp.com)

#### [Django admin](https://eventstud.herokuapp.com/admin)

```text
username: eventstud
password: admineventstud
```

<br>

### Panduan untuk kelompok

**Lihat *branch* [`dev`][dev] untuk perkembangan yang paling baru.**

Untuk mengerjakan, silakan ikuti langkah-langkah berikut.

1. ***Clone*** repositori ini dengan `git clone https://gitlab.com/PPW-D4/eventstud.git`.
2. **Aktifkan** *virtual environment* kalian untuk proyek ini (buat baru jika belum ada).
3. Masuk ke direktori repositori yang baru saja di-*clone* (gunakan **`cd`**).
4. Masukkan perintah berikut:

```
git checkout dev
git checkout -b nama_kalian
```

Contoh:

```
git checkout dev
git checkout -b sage
```

Sebenarnya nama *branch* bebas saja, jika ingin menggunakan *branch* pada tugas 1 juga boleh.
Penggunaan nama kalian sebagai nama branch hanya untuk mempermudah dalam membedakan *branch*
mana milik siapa. Lebih baik apabila nama *branch* disesuaikan dengan nama fitur yang dibuat
(seperti pada tugas 1), tetapi berhubung fitur kita sangat erat pada tugas 2, saran ini bisa
diabaikan.

5. Cek **`git status`**. Jika sudah muncul `On branch nama_kalian`, berarti sudah oke.

6. Silakan kerjakan sesuai pembagian tugas masing-masing. Sebelum *commit*, usahakan untuk
   menjalankan perintah **`git rebase origin/dev`** supaya tidak perlu **pull** lagi nantinya.
   Namun jika terlanjur, tidak apa, cukup lakukan **`git pull origin dev`** apabila terjadi
   kendala saat melakukan **`push`**.

7. Untuk push, silakan gunakan perintah **`git push origin nama_branch`**.


Terkait **static files**, untuk:
- gambar, silakan letakkan di folder **`img`**;
- css, silakan letakkan di folder **`css`**;
- javascript, silakan letakkan di folder **`js`**.

Selamat mengerjakan!

[home]: https://gitlab.com/PPW-D4/eventstud/commits/home
[newspage]: https://gitlab.com/PPW-D4/eventstud/commits/newspage
[events]: https://gitlab.com/PPW-D4/eventstud/commits/events
[register]: https://gitlab.com/PPW-D4/eventstud/commits/register
[dinda]: https://gitlab.com/PPW-D4/eventstud/commits/dinda
[saffa]: https://gitlab.com/PPW-D4/eventstud/commits/saffa
[events_v2]: https://gitlab.com/PPW-D4/eventstud/commits/events-v2
[about_us]: https://gitlab.com/PPW-D4/eventstud/commits/about_us
[dev]: https://gitlab.com/PPW-D4/eventstud/tree/dev

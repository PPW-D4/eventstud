from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import index
from .models import Register_Member
from .forms import RegisterMember

# Create your tests here.
class RegisterMemberUnitTest(TestCase):

    def test_tp1_url_is_exist(self):
        response = Client().get('/registermember/')
        self.assertEqual(response.status_code, 200)

    def test_tp1_using_index_func(self):
        found = resolve('/registermember/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_name(self):
        inputRegister = Register_Member.objects.create(name='jih', username='jihanocta_', email='jih@gmail.com', birthdate='2018-10-12', password='234', address='jl fasilkom')
        counting_all_input_register = Register_Member.objects.all().count()
        self.assertEqual(counting_all_input_register, 1)

    def test_form_validation_for_name_blank_items(self):
        form = RegisterMember(data={'name': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )

    def test_form_post_and_render_the_result(self):
        test = 'Anonymous'
        username = 'jihanocta_'
        email = 'jihanocta@studentunion.com'
        birthdate = '1999-10-28'
        password = '234'
        address = 'jalan fasilkom'
        response_post = Client().post('/registermember/', {'name':test, 'username':username, 'email':email, 'birthdate':birthdate, 'password':password, 'address':address})
        self.assertEqual(response_post.status_code, 302)

    def test_self_func(self):
        inputRegister = Register_Member.objects.create(name='JIHAN', username='jihanocta_', email='jih@gmail.com', birthdate='2018-10-12', password='234', address='jl fasilkom')
        status = Register_Member.objects.get(name='JIHAN')
        self.assertEqual(str(status), status.name)

    def test_text_max_name_length(self):
        nama = Register_Member.objects.create(name='jih', username='jihanocta_', email='jih@gmail.com', birthdate='2018-10-12', password='234', address='jl fasilkom')
        self.assertLessEqual(len(str(nama)), 30)
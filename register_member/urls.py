from django.urls import path
from .views import index

app_name = 'register_member'
urlpatterns = [
    path('', index, name="index"),
]
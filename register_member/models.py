from django.db import models

# Create your models here.
class Register_Member(models.Model):
    name = models.CharField(max_length=27)
    username = models.CharField(unique = True, error_messages = {'unique': "Username must to be unique"}, max_length=27)
    email = models.CharField(unique = True, error_messages = {'unique': "Email must to be unique"}, max_length=27)
    birthdate = models.DateField(max_length=27)
    password = models.CharField(max_length=27)
    address = models.CharField(max_length=50)

    def __str__(self):
        return self.name
    
from django.shortcuts import render
from .forms import RegisterMember
from .models import Register_Member
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = RegisterMember(request.POST)
        print(form.errors)
        if form.is_valid():
            data = form.cleaned_data
            Register_Member.objects.create(**data)
            return HttpResponseRedirect('/')
    else:
        form = RegisterMember()
    result = list(Register_Member.objects.all())
    result.reverse()
    content = {'nama': result, 'form' : form}
    return render(request, 'newMember.html', content)
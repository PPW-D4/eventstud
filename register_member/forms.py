from django import forms

class RegisterMember(forms.Form):
    name = forms.CharField(label = 'NAME:', required=True, max_length=27, widget=forms.TextInput)
    username = forms.CharField(label = 'USERNAME:', required=True, max_length=27, widget=forms.TextInput)
    email = forms.CharField(label = 'EMAIL:', required=True, max_length=27, widget=forms.EmailInput)
    birthdate = forms.DateField(label = 'BIRTHDATE:', required=True, widget=forms.DateInput(attrs={'type':'date'}))
    password = forms.CharField(label = 'PASSWORD:', required=True, max_length=27, widget=forms.PasswordInput)
    address = forms.CharField(label = 'ADDRESS:', required=True, max_length=50, widget=forms.TextInput)
	
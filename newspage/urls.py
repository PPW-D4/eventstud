from .views import tampilNews
from .views import beritaFull
from django.conf.urls import url

app_name = 'newspage'
urlpatterns = [
	url(r'^$', tampilNews, name = 'tampilNews'),
	url(r'^(?P<nomor>.*)/$', beritaFull, name = 'beritaFull'),
]
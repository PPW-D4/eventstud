from django.db import models

# Create your models here.
class objekNews(models.Model) :
	judul = models.CharField(max_length = 200)
	waktu = models.DateField()
	linkfoto = models.CharField(max_length = 200, blank = True)
	shortdesc = models.CharField(max_length = 1000)
	content = models.CharField(max_length = 4000)
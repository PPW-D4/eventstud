from django.shortcuts import render
from .models import objekNews

# Create your views here.
response = {'author' : 'Saffanah Fausta Lamis'}
def tampilNews(request):
	response['datanews'] = objekNews.objects.all()
	return render(request, 'newspage/pageNews.html', response)
def beritaFull(request, nomor):
	response['objek'] = objekNews.objects.get(id=nomor)
	return render(request, 'newspage/berita-template.html', response)

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import tampilNews
from .views import beritaFull

# Create your tests here.
class NewspageUnitTest(TestCase):
	def test_urlnews_is_exist(self):
		response = Client().get('/news/')
		self.assertEqual(response.status_code, 200)

	def test_tampilnews_is_exist(self):
		found = resolve('/news/')
		self.assertEqual(found.func, tampilNews)



